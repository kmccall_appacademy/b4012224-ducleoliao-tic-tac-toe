class Board
  attr_accessor :grid, :mark

  def initialize(grid=Array.new(3) { Array.new(3) })
    @grid = grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    if win_row?(:X) || win_column?(:X) || win_diagonal?(:X)
      :X
    elsif win_row?(:O) || win_column?(:O) || win_diagonal?(:O)
      :O
    else
      nil
    end
  end

  def win_row?(mark)
    grid.any? { |row| row.all? { |el| el == mark } }
  end

  def win_column?(mark)
    grid.transpose.any? { |column| column.all? { |el| el == mark } }
  end

  def win_diagonal?(mark)
    (0...grid.length).all? { |idx| grid[idx][idx] == mark } || (0...grid.length).all? { |i| grid[i][(grid.length - 1) - i] == mark }
  end

  def over?
    @grid.flatten.none? { |block| block.nil? } || winner
  end

  def render
    puts "#{@grid[0][0]} | #{@grid[0][1]} | #{@grid[0][2]}"
    puts "---------"
    puts "#{@grid[1][0]} | #{@grid[1][1]} | #{@grid[1][2]}"
    puts "---------"
    puts "#{@grid[2][0]} | #{@grid[2][1]} | #{@grid[2][2]}"
    puts "---------"
  end

end
