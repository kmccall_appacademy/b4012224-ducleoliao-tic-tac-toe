require "board"
require "byebug"

class ComputerPlayer
attr_accessor :mark, :name

  def initialize(name)
    @name = name
  end

  def board
    @board
  end

  def get_move
    available_cell.each do |pos|
      return pos if potential_win?(pos)
    end
    available_cell.sample
  end

  def potential_win?(pos)
    board[pos] = mark
    if board.winner
      board[pos] = nil
      true
    else
      board[pos] = nil
      false
    end
  end

  def available_cell
    available = []
    (0..2).each do |row|
      (0..2).each do |col|
        pos = row, col
        available << pos if board.empty?(pos)
      end
    end
    available
  end

  def display(board)
    @board = board
  end

end
