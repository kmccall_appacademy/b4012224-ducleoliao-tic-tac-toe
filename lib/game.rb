require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
require "byebug"

class Game
attr_accessor :player_one, :player_two, :board

  def initialize(player_one, player_two)
    @board = Board.new
    @player_one = ComputerPlayer.new(player_one)
    @player_two = ComputerPlayer.new(player_two)
    player_one.mark = :X
    player_two.mark = :O
    @current_player = @player_one.name
  end

  def current_player
    @current_player
  end

  def switch_players!
    if @current_player == @player_one.name
      @current_player = @player_two.name
    else
      @current_player = @player_one.name
    end
  end

  def play_turn

    next_move = @current_player.get_move
    # 
    # until @board.empty?(next_move)
    #   puts "The move was invalid, please try again."
    #   next_move = @current_player.get_move
    # end

    @board.place_mark(next_move, @current_player.mark)
    switch_players!
  end

  def play
    until @board.over?
      @board.display
      play_turn
      switch_players!
    end

    if @board.winner.nil?
      puts "No winner, let's try again!"
    else
      switch_players!
      puts "#{@current_player.name} is the winner!"
    end
    @board.display
  end

end
