require "board"

class HumanPlayer
attr_accessor :name, :mark

  def initialize(name)
    @name = name
  end

  def get_move
    print "where"
    input = gets.chomp
    input.split(", ").map! {|num| num.to_i }
  end

  def display(board)
    board.render
  end

end
